# Sticker converter
# Copyright (C) 2023  Frisk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import re

import requests
import json
import os
from typing import List
from typing import Any
from dataclasses import dataclass

sticker_packs = {}
@dataclass
class Author:
    name: str
    link: str

    @staticmethod
    def from_dict(obj: Any) -> 'Author':
        if obj is None:
            return None
        _name = str(obj.get("name"))
        _link = str(obj.get("link"))
        return Author(_name, _link)

@dataclass
class ThumbnailInfo:
    mimetype: str
    h: int
    w: int

    @staticmethod
    def from_dict(obj: Any) -> 'ThumbnailInfo':
        _mimetype = str(obj.get("mimetype"))
        _h = int(obj.get("h"))
        _w = int(obj.get("w"))
        return ThumbnailInfo(_mimetype, _h, _w)

@dataclass
class Info:
    h: int
    w: int
    mimetype: str
    thumbnail_info: ThumbnailInfo
    thumbnail_url: str
    author: Author | None

    @staticmethod
    def from_dict(obj: Any) -> 'Info':
        _h = int(obj.get("h"))
        _w = int(obj.get("w"))
        _mimetype = str(obj.get("mimetype"))
        _thumbnail_info = ThumbnailInfo.from_dict(obj.get("thumbnail_info"))
        _thumbnail_url = str(obj.get("thumbnail_url"))
        _author = Author.from_dict(obj.get("author"))
        return Info(_h, _w, _mimetype, _thumbnail_info, _thumbnail_url, _author)

    def to_dict(self) -> dict:
        return dict(w=self.w, h=self.h, mimetype=self.mimetype)

@dataclass
class Sticker:
    body: str
    info: Info
    url: str
    msgtype: str
    id: str

    @staticmethod
    def from_dict(obj: Any) -> 'Sticker':
        _body = str(obj.get("body"))
        _info = Info.from_dict(obj.get("info"))
        _url = str(obj.get("url"))
        _msgtype = str(obj.get("msgtype"))
        _id = str(obj.get("id"))
        return Sticker(_body, _info, _url, _msgtype, _id)

    def to_dict(self) -> dict:
        return dict(body=self.body, info=self.info.to_dict(), url=self.url)


@dataclass
class Root:
    title: str
    id: str
    stickers: List[Sticker]

    @staticmethod
    def from_dict(obj: Any) -> 'Root':
        _title = str(obj.get("title"))
        _id = str(obj.get("id"))
        _stickers = [Sticker.from_dict(y) for y in obj.get("stickers")]
        return Root(_title, _id, _stickers)

    def get_attribution(self) -> str:
        for sticker in self.stickers:
            if sticker.info.author is not None:
                return sticker.info.author.name
        return "Unattributed"

    def to_json(self) -> str:
        fresh_dict = {}
        first_sticker = self.stickers[0]
        fresh_dict["pack"] = {}
        fresh_dict["pack"]["avatar_url"] = first_sticker.url
        fresh_dict["pack"]["display_name"] = self.title
        fresh_dict["pack"]["usage"] = ["sticker"]
        fresh_dict["pack"]["attribution"] = self.get_attribution()
        fresh_dict["images"] = {}
        for sticker in self.stickers:
            # Sticker name will be anything before a new line or "–" EN DASH character, if neither are found then anything before first space bar becomes sticker name
            # Worth mentioning that this doesn't strip special characters, so you might want to do this by yourself
            name = re.search(r"(.*?)(\u2013|\\n)", sticker.body)
            name = name.group(1) if name is not None and name.groups() else sticker.body.split(" ")[0]
            name = name.strip().replace(" ", "").lower()
            if name in fresh_dict["images"]:
                name = name + str(len([x for x in fresh_dict["images"] if x.startswith(name)])+1)
            fresh_dict["images"][name] = sticker.to_dict()
        return json.dumps(fresh_dict, indent=4)


if not (os.path.isdir('stickers') and os.path.isdir('output')):
    print("stickers or output directory not found, creating directories, please add your sticker files to stickers directory.")
    os.makedirs("stickers")
    os.makedirs("output")

for dirname, _, names in os.walk("stickers"):
    for pack in names:
        with open("stickers/"+pack, "r") as single_fh:
            sticker_pack = Root.from_dict(json.load(single_fh))
            sticker_packs[sticker_pack.title] = sticker_pack.to_json()


for pack, pack_data in sticker_packs.items():
    if os.path.isfile("output/"+pack+".json"):
        continue
    with open("output/"+pack+".json", "w") as sticker_pack_out:
        sticker_pack_out.write(pack_data)


